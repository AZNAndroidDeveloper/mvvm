package uz.azn.mvvmproject

import androidx.lifecycle.ViewModel

class IntroViewModel:ViewModel() {
    var number = 0
    fun increment(){
        if (number != 10)
        number++
    }
    fun decrement(){
        if (number !=0)
        number--
    }
}