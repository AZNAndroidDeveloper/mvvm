package uz.azn.mvvmproject

import android.os.CountDownTimer
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class Intro1ViewModel : ViewModel() {

    private val username = MutableLiveData<String>()
    private lateinit var countDownTimer: CountDownTimer
    private var time = MutableLiveData<Int>()

    fun setUserName(name: String) {
        username.value =
            name //  countin ichida ishlata olmaymiz postValue ishlatamiz  yani bu ui da ozgartirish qilib berardi
        // postValue esa backgrounda ozgartirish qiladi

    }

    fun getUserName(): LiveData<String> {
        return username

    }

    fun startTime() {
        countDownTimer = object : CountDownTimer(15000, 1000) {
            override fun onFinish() {

            }

            override fun onTick(millisUntilFinished: Long) {
                val secondLeft = millisUntilFinished / 1000
                time.value = secondLeft.toInt()

            }

        }.start()
    }

    fun stopTimer() {
        countDownTimer.cancel()

    }

    fun getTime():LiveData<Int>{
        return   time
    }
}