package uz.azn.mvvmproject

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import uz.azn.mvvmproject.databinding.FragmentIntroBinding

class IntroFragment : Fragment(R.layout.fragment_intro) {
    private lateinit var binding: FragmentIntroBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        val viewModel = ViewModelProvider(requireActivity()).get(IntroViewModel::class.java)
        with(binding){
            textView.text = viewModel.number.toString()
            btnIncrement.setOnClickListener {
                viewModel.increment()
                textView.text = viewModel.number.toString()
            }
            btnDecrement.setOnClickListener {
                viewModel.decrement()
                textView.text = viewModel.number.toString()
            }
        }
    }

}