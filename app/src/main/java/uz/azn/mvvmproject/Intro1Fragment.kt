package uz.azn.mvvmproject

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import uz.azn.mvvmproject.databinding.FragmentIntro1Binding

class Intro1Fragment : Fragment(R.layout.fragment_intro1) {
    private lateinit var binding: FragmentIntro1Binding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntro1Binding.bind(view)
        val viewModel = ViewModelProvider(this).get(Intro1ViewModel::class.java)

        viewModel.startTime()
        viewModel.getTime().observe(requireActivity(), Observer {
            with(binding){
                textView.text = it.toString()
            }
        })
//        viewModel.setUserName("Live Data")
//        viewModel.getUserName().observe(requireActivity(), Observer {
//            with(binding) {
//                textView.text = it
//            }
//        })
//        Handler().postDelayed({
//            viewModel.setUserName("Zokirjon")
//        }, 2000)

    }

}